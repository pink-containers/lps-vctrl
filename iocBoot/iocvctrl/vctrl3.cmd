#!../../bin/linux-x86_64/vctrl

## You may have to change vctrl to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/vctrl.dbd"
vctrl_registerRecordDeviceDriver pdbbase

epicsEnvSet ("STREAM_PROTOCOL_PATH", ".")

drvAsynIPPortConfigure("L1","$(DEVIP):$(DEVPORT)",0,0,0)
#drvAsynIPPortConfigure("L1","127.0.0.1:50123",0,0,0)

## Load record instances
dbLoadRecords("${EXSUB}/db/exsub.db","P=$(IOCBL):,R=$(IOCDEV):exsub")
dbLoadRecords("${ASYN}/db/asynRecord.db","P=$(IOCBL):,R=$(IOCDEV):asyn,PORT=L1,ADDR=0,IMAX=256,OMAX=256")
dbLoadRecords("db/vctrl3.db", "BL=$(IOCBL),DEV=$(IOCDEV),PORT=L1")
#dbLoadRecords("db/vctrl4.db", "BL=$(IOCBL),DEV=$(IOCDEV),PORT=L1")

cd "${TOP}/iocBoot/${IOC}"

dbLoadRecords("toggle3.db", "BL=$(IOCBL),DEV=$(IOCDEV)")

set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings3.sav")

iocInit

create_monitor_set("auto_settings3.req", 30, "BL=$(IOCBL),DEV=$(IOCDEV)")

## Start any sequence programs
#seq sncxxx,"user=epics"
