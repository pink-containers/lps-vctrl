# LPS-vctrl

EPICS IOC and container for the LPS vacuum controller

## Docker-compose.yml
```yml
version: "3.7"
services:
  ioc:
    image: docker.gitlab.gwdg.de/pink-containers/lps-vctrl/lpsvctrl:latest
    container_name: ct1
    network_mode: host
    stdin_open: true
    tty: true
    restart: always
    working_dir: /EPICS/IOCs/lps-vctrl/iocBoot/iocvctrl
    command: "./vctrl3.cmd"
    volumes:
      - type: bind
        source: /EPICS/autosave/ct1
        target: /EPICS/autosave
    environment:
      - DEVIP=0.0.0.0
      - DEVPORT=23
      - IOCBL=LPQ
      - IOCDEV=ct1
```
